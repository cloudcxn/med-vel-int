# medvelint

Medidor de Velocidad de Internet

Este programa tiene sus bases en:
[How to use speedtest on a Linux server to check, store and report internet
speeds graphically](https://www.howtoforge.com/tutorial/how-to-use-speedtest-on-a-linux-server-to-check-store-and-report-internet-speeds-graphically/).

Inicialmente este programa usaba [speedtest-cli de Sivel](https://github.com/sivel/speedtest-cli).
Sin embargo, ahora se usa [speedtest-cli de Ookla](https://www.speedtest.net/es/apps/cli)
debido a un mejor desempeño y soporte, aunque este programa no es de código
abierto.

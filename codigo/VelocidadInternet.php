<?php declare(strict_types=1);

class VelocidadInternet
{
    private $carga = array();
    private $descarga = array();
    private $interfaz = array();
    private $medicion = array();
    private $ping = array();
    private $resultado = array();
    private $servidor = array();

    public function __construct(string $medicion)
    {
        $this->analizarCadena($medicion);
    }

    private function analizarCadena(string $medicion)
    {
        $datos = json_decode($medicion, true);

        $this->carga['ancho_banda'] = $datos['upload']['bandwidth'];
        $this->carga['bytes'] = $datos['upload']['bytes'];
        $this->carga['transcurrido'] = $datos['upload']['elapsed'];

        $this->descarga['ancho_banda'] = $datos['download']['bandwidth'];
        $this->descarga['bytes'] = $datos['download']['bytes'];
        $this->descarga['transcurrido'] = $datos['download']['elapsed'];

        $this->interfaz['es_vpn'] = $datos['interface']['isVpn'];
        $this->interfaz['ip_externa'] = $datos['interface']['externalIp'];
        $this->interfaz['ip_interna'] = $datos['interface']['internalIp'];
        $this->interfaz['mac'] = $datos['interface']['macAddr'];
        $this->interfaz['nombre'] = $datos['interface']['name'];

        $this->medicion['fecha'] = $datos['timestamp'];
        $this->medicion['isp'] = $datos['isp'];
        $this->medicion['tipo'] = $datos['type'];

        $this->ping['fluctuacion'] = $datos['ping']['jitter'];
        $this->ping['latencia'] = $datos['ping']['latency'];

        $this->resultado['id'] = $datos['result']['id'];
        $this->resultado['url'] = $datos['result']['url'];

        $this->servidor['huesped'] = $datos['server']['host'];
        $this->servidor['id'] = $datos['server']['id'];
        $this->servidor['ip'] = $datos['server']['ip'];
        $this->servidor['nombre'] = $datos['server']['name'];
        $this->servidor['pais'] = $datos['server']['country'];
        $this->servidor['puerto'] = $datos['server']['port'];
        $this->servidor['ubicacion'] = $datos['server']['location'];
    }

    public function obtenerCarga()
    {
        return $this->carga;
    }

    public function obtenerDescarga()
    {
        return $this->descarga;
    }

    public function obtenerInterfaz()
    {
        return $this->interfaz;
    }

    public function obtenerMedicion()
    {
        return $this->medicion;
    }

    public function obtenerPing()
    {
        return $this->ping;
    }

    public function obtenerResultado()
    {
        return $this->resultado;
    }

    public function obtenerServidor()
    {
        return $this->servidor;
    }

}

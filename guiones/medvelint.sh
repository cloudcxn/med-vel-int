#!/bin/bash
# Ejecuta pruebas de velocidad hacia 3 servidores y envia los resultados a un
# programa PHP.
#
# Ejecutado una vez cada hora por un trabajo cron

if [[ $EUID -ne 0 ]]; then
   echo "medvelint debe ser ejecutado como raíz"
   exit 1
fi

source /opt/medvelint/libmedvelint.sh

main () {
  servidores[1]=18800  # Quito
  servidores[2]=10390  # New York
  servidores[3]=14979  # Madrid

  for i in ${servidores[@]}; do
    local medicion=$(ejecutar_medicion $i)
    php /opt/medvelint/RegistrarMedicion.php "$medicion"
  done
}

main

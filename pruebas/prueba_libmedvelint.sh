#!/bin/bash
source $1/externos/assert.sh
source $1/lib/libmedvelint.sh

# Probar que speedtest-cli está instalado.
# Se espera que el código de salida de 'speedtest --version' sea 0.
assert_raises "speedtest --version"

# Probar que ejecutar_medicion() finaliza de forma correcta.
# Se espera que el código de salida de 'ejecutar_medicion 18800' sea 0.
assert_raises "ejecutar_medicion 18800"

# Probar la impresión de ejecutar_medicion().
# Se espera que la impresión de 'ejecutar_medicion 18800' sea una cadena de
# caracteres con formato JSON cuyos tres primeros valores sean
# '"type":"result","timestamp"'
assert_raises "ejecutar_medicion 18800 | grep '\"type\":\"result\",\"timestamp\"'"

# Fin del paquete de pruebas.
assert_end lib_medvelint

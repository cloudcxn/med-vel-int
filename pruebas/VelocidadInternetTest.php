<?php declare(strict_types=1);
use PHPUnit\Framework\TestCase;

final class VelocidadInternetTest extends TestCase
{
    private $medicion = '{"type":"result","timestamp":"2020-04-26T19:35:03Z",'
        . '"ping":{"jitter":0.055,"latency":139.261},"download":{'
        . '"bandwidth":174248616,"bytes":2123395608,"elapsed":15003},'
        . '"upload":{"bandwidth":62783595,"bytes":584923995,"elapsed":10003},'
        . '"isp":"Google Cloud","interface":{"internalIp":"172.17.0.2",'
        . '"name":"eth0","macAddr":"02:42:AC:11:00:02","isVpn":false,'
        . '"externalIp":"172.217.8.142"},"server":{"id":21800,'
        . '"name":"Movistar Ecuador","location":"Quito","country":"Ecuador",'
        . '"host":"speedtest.movistar.com.ec","port":8080,"ip":"200.7.236.134"},'
        . '"result":{"id":"4ca9d130-44cf-44d6-b6a4-aa310b23a0ex",'
        . '"url":"https://www.speedtest.net/result/c/4ca9d130-44cf-44d6-b6a4-aa310b23a0ex"}}';

    public function testAnalizarCarga(): void
    {
        $vel_int = new VelocidadInternet($this->medicion);
        $carga = $vel_int->obtenerCarga();

        $this->assertEquals('62783595', $carga['ancho_banda']);
        $this->assertEquals('584923995', $carga['bytes']);
        $this->assertEquals('10003', $carga['transcurrido']);
    }

    public function testAnalizarDescarga(): void
    {
        $vel_int = new VelocidadInternet($this->medicion);
        $descarga = $vel_int->obtenerDescarga();

        $this->assertEquals('174248616', $descarga['ancho_banda']);
        $this->assertEquals('2123395608', $descarga['bytes']);
        $this->assertEquals('15003', $descarga['transcurrido']);
    }

    public function testAnalizarInterfaz(): void
    {
        $vel_int = new VelocidadInternet($this->medicion);
        $interfaz = $vel_int->obtenerInterfaz();

        $this->assertEquals(false, $interfaz['es_vpn']);
        $this->assertEquals('172.217.8.142', $interfaz['ip_externa']);
        $this->assertEquals('172.17.0.2', $interfaz['ip_interna']);
        $this->assertEquals('02:42:AC:11:00:02', $interfaz['mac']);
        $this->assertEquals('eth0', $interfaz['nombre']);
    }

    public function testAnalizarMedicion(): void
    {
        $vel_int = new VelocidadInternet($this->medicion);
        $medicion = $vel_int->obtenerMedicion();

        $this->assertEquals('2020-04-26T19:35:03Z', $medicion['fecha']);
        $this->assertEquals('Google Cloud', $medicion['isp']);
        $this->assertEquals('result', $medicion['tipo']);
    }

    public function testAnalizarPing(): void
    {
        $vel_int = new VelocidadInternet($this->medicion);
        $ping = $vel_int->obtenerPing();

        $this->assertEquals(0.055, $ping['fluctuacion']);
        $this->assertEquals(139.261, $ping['latencia']);
    }

    public function testAnalizarResultado(): void
    {
        $vel_int = new VelocidadInternet($this->medicion);
        $resultado = $vel_int->obtenerResultado();

        $this->assertEquals('4ca9d130-44cf-44d6-b6a4-aa310b23a0ex', $resultado['id']);
        $this->assertEquals('https://www.speedtest.net/result/c/4ca9d130-44cf-44d6-b6a4-aa310b23a0ex', $resultado['url']);
    }

    public function testAnalizarServidor(): void
    {
        $vel_int = new VelocidadInternet($this->medicion);
        $servidor = $vel_int->obtenerServidor();

        $this->assertEquals('speedtest.movistar.com.ec', $servidor['huesped']);
        $this->assertEquals(21800, $servidor['id']);
        $this->assertEquals('200.7.236.134', $servidor['ip']);
        $this->assertEquals('Movistar Ecuador', $servidor['nombre']);
        $this->assertEquals('Ecuador', $servidor['pais']);
        $this->assertEquals(8080, $servidor['puerto']);
        $this->assertEquals('Quito', $servidor['ubicacion']);
    }

}
